#Version 1.0

FROM ubuntu:22.10

ADD *.sh /tmp/

RUN chmod a+x /tmp/*.sh && /tmp/creation.sh
RUN apt-get update && apt install man nano vim -y && yes | unminimize

CMD ["sleep", "infinity"]
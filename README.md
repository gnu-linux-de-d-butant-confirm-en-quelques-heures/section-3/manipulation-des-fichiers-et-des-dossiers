# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de manipuler les fichiers et les répertoires sur Linux.

Ainsi nous verrons les commandes suivantes:
- `touch`: commande permettant de créer un fichier
- `rm`: commande permettant de supprimer un fichier
- `mkdir`: commande permettant de créer un répertoire
- `rmdir`: commande permettant de supprimer un répertoire


<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --name man jassouline/file1:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

Dans ce premier exercice, nous allons essayer de comprendre comment utiliser la commande `touch` de manière simple.

La commande `touch` nous permet de créer des nouveaux fichiers.

1. Utilisez la commande `touch` pour créer les fichiers suivants:
- `/tmp/exercices/livres/first.file`
- `/tmp/exercices/livres/second.file`

2. Vérifiez que vos commandes ont bien fonctionnées grâce à la commande `ls /tmp/exercices/livres/`

Vous devez voir les deux fichiers que vous avez créé (en plus de ceux déjà existants)

3. Créez un fichier nommé `Author.md` dans le répertoire `/tmp/exercices/animaux` qui contient le texte suivant :
**ASSOULINE Jordan**

<details><summary> Montrer la solution </summary>
<p>
touch /tmp/exercices/livres/first.file
</p>
<p>
touch /tmp/exercices/livres/second.file
</p>
<p>
echo "ASSOULINE Jordan" > /tmp/exercices/animaux/Author.md
</p>
</details>

4. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/creation_touch_verify.sh`

Si vous obtenez le message : `OK !` c'est que les fichiers ont été correctement créés.

<br/>

# Exercice 2

Dans ce deuxième exercice, nous allons essayer de comprendre comment utiliser les commandes `touch` et `mkdir` de manière simple.

La commande `mkdir` nous permet de créer de nouveaux répertoires.

1. Utilisez la commande `mkdir` pour créer les répertoires suivants:
- `/tmp/exercices/livres/scifi`
- `/tmp/exercices/livres/fantasy`

Vérifiez que vos commandes ont bien fonctionnées grâce à la commande `ls -l /tmp/exercices/livres/`. 

Vous devez voir les deux répertoires que vous avez créé (en plus des fichiers existants)

2. Créez un fichier nommé `Author.md` dans le répertoire `/tmp/exercices/livres/fantasy`

<details><summary> Montrer la solution </summary>
<p>
mkdir /tmp/exercices/livres/scifi
</p>
<p>
mkdir /tmp/exercices/livres/fantasy
</p>
<p>
touch /tmp/exercices/livres/fantasy/Author.md
</p>
</details>

3. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/creation_mkdir_verify.sh`

Si vous obtenez le message : `OK !` c'est que les fichiers ont été correctement créés.

<br/>

# Exercice 3

Dans ce troisième exercice, nous allons essayer de comprendre comment utiliser les commandes `rm` et `rmdir` de manière simple.

La commande `rmdir` nous permet de supprimer un répertoire, de même que la commande `rm` nous permet de supprimer un fichier.

1. Utilisez la commande `rm` pour supprimer tous les fichiers (pas les répertoires) situés dans le répertoire `/tmp/exercices/livres`

*Vous pouvez utiliser la commande `ls` pour vérifier les fichiers à supprimer*

<details><summary> Montrer la solution </summary>
<p>
cd /tmp/exercices/livres/
</p>
<p>
ls
</p>
<p>
rm CKAD.book first.file second.file LPIC.pdf
</p>
</details>

**Q2: Combien de fichiers avez-vous supprimé ?**

<details><summary> Montrer la solution </summary>
<p>
Nous avons supprimé 4 fichiers.
</p>
</details>

2. Essayez de supprimer le dossier `/tmp/exercices/livres/scifi` grâce à la commande `rmdir`

<details><summary> Montrer la solution </summary>
<p>
rmdir /tmp/exercices/livres/scifi
</p>
</details>

3. Essayez de supprimer le dossier `/tmp/exercices/livres/fantasy` grâce à la commande `rmdir /tmp/exercices/livres/fantasy`

4. Vérifiez si le répertoire a bien été supprimé grâce à la commande `ls /tmp/exercices/livres/`

**Q3: Pourquoi cela ne fonctionne pas ?**
- [ ] Car je ne possède pas les droits nécessaires
- [ ] Car le dossier n'existe pas
- [ ] Car le dossier contient le fichier Author.md
- [ ] Car la commande est fausse

<details><summary> Montrer la solution </summary>
<p>
Car le dossier contient le fichier Author.md
</p>
</details>

5. Maintenant refaîtes de même avec la commande `rm -Rf /tmp/exercices/livres/fantasy` et vérifiez avec la commande `ls /tmp/exercices/livres/`

**Q4: Est-ce que cela a fonctionné ?**
- [ ] Oui
- [ ] Non

<details><summary> Montrer la solution </summary>
<p>
Oui le répertoire a bien été supprimé
</p>
</details>

6. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/suppression_verify.sh`

Si vous obtenez le message : `OK !` c'est que les fichiers ont été correctement créés.

<br/>

# Conclusion

Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`


#Creation de l'arborescence
mkdir /tmp/exercices
mkdir /tmp/exercices/livres
mkdir /tmp/exercices/animaux
mkdir /tmp/exercices/GNU
mkdir /tmp/exercices/GNU/linux

#Création des fichiers
touch /tmp/exercices/livres/CKAD.book
touch /tmp/exercices/livres/LPIC.pdf
touch /tmp/exercices/livres/.cache.epub

touch /tmp/exercices/animaux/chat
touch /tmp/exercices/animaux/chien
touch /tmp/exercices/animaux/cheval
touch /tmp/exercices/animaux/singe
touch -a -m -t 201512180130.09 /tmp/exercices/animaux/singe #Modifie la date du fichier
touch -a -m -t 201512200130.09 /tmp/exercices/animaux/chien #Modifie la date du fichier

cp /etc/passwd /tmp/exercices/GNU/linux/ubuntu.file
touch /tmp/exercices/GNU/linux/debian.file
touch /tmp/exercices/GNU/linux/centos.file


